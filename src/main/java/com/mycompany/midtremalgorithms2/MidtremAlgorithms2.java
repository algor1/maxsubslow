/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.midtremalgorithms2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class MidtremAlgorithms2 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int size = kb.nextInt();
        int[] arr = new int[size]; 
        for (int i = 0 ; i<size;i++){
            arr[i]= kb.nextInt();
        }
        int m = Integer.MIN_VALUE;
        for(int j = 0 ; j <= arr.length;j++){
             for(int k = j ; k <=arr.length;k++){
                int s = 0;
                for(int i = j ; i < k ; i++){
                    s = s + arr[i];
                }
                if (s > m){
                    m = s;
                }
             }
        }
        System.out.println("Input : " +Arrays.toString(arr));
        System.out.println("MaxSub :"+m);
    }
}
